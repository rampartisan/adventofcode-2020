from collections import namedtuple
import numpy as np
import os
import collections
from itertools import product

TEST = False


class CCube():
    def __init__(self, state, neighbours):
        self.state = state
        self.neighbour_active_count = 0
        self.neighbours = neighbours

    def get_state(self):
        return self.state

    def inc_neighbour_active_count(self):
        self.neighbour_active_count += 1

    def clear_neighbour_active_count(self):
        self.neighbour_active_count = 0

    def get_neighbours(self):
        return self.neighbours

    def calc_state(self):
        if self.state:
            if self.neighbour_active_count == 2 or self.neighbour_active_count == 3:
                pass
            else:
                self.state = False
        else:
            if self.neighbour_active_count == 3:
                self.state = True


class Universe:

    def __init__(self, blueprint, ndim):

        self.ticks_since_epoch = 0
        self.cubes = {}
        self.dims = [range(len(blueprint)), range(len(blueprint[0]))]
        for _ in range(ndim - 2):
            self.dims.append(range(1))

        for pos in product(*self.dims):
            state = True if blueprint[pos[0]][pos[1]] == '#' else False
            self.cubes[pos] = CCube(
                state, self.calc_neighbours_for_position(pos))

    def calc_neighbours_for_position(self, p):
        ndim = len(self.dims)
        offset_idx = np.indices((3,) * ndim).reshape(ndim, -1).T
        offsets = np.r_[-1, 0, 1].take(offset_idx)
        offsets = offsets[np.any(offsets, 1)]
        return p + offsets

    def expand(self):
        for i in range(len(self.dims)):
            self.dims[i] = range(self.dims[i].start - 1, self.dims[i].stop + 1)
            max_cur_dim = self.dims[i].stop - 1
            min_cur_dim = self.dims[i].start
            for p in product(*(self.dims[: i] + self.dims[i+1:])):
                plist = list(p)
                plist.insert(i, max_cur_dim)
                self.cubes[tuple(plist)] = CCube(
                    False, self.calc_neighbours_for_position(plist))
                plist[i] = min_cur_dim
                self.cubes[tuple(plist)] = CCube(
                    False, self.calc_neighbours_for_position(plist))

    def tick(self):
        self.ticks_since_epoch += 1

        self.expand()

        for c in self.cubes.values():
            for n in c.get_neighbours():
                if tuple(n) in self.cubes:
                    if self.cubes[tuple(n)].get_state():
                        c.inc_neighbour_active_count()

        for c in self.cubes.values():
            c.calc_state()
            c.clear_neighbour_active_count()

    def get_active_cube_count(self):
        count = 0
        for c in self.cubes.values():
            if c.get_state():
                count += 1
        return count


with open(os.path.dirname(__file__)+"/17.txt") as f:
    blueprint = [[char for char in line.replace('\n', '')] for line in f]

u = Universe(blueprint, 3)
for _ in range(6):
    u.tick()
print("P1:\n{}".format(u.get_active_cube_count()))

u = Universe(blueprint, 4)  # takes fucking ages. dicts were a mistake.
for i in range(6):
    print("tick {}".format(i+1))
    u.tick()
print("P2:\n{}".format(u.get_active_cube_count()))


def test():
    bp = ".#.\n..#\n###"
    uni = Universe(bp, 3)
    for _ in range(6):
        uni.tick()
    assert(uni.get_active_cube_count() == 112)


if TEST:
    test()
