import os
import re
from collections import namedtuple
import numpy as np

Ticket_Field = namedtuple('Ticket_Field', 'name range1 range2')
field_re = re.compile("(.+):\s(\d+)-(\d+)\sor\s(\d+)-(\d+)")
fields = []

script_dir = os.path.dirname(__file__)
with open(script_dir+"/16.txt") as f:
    data = f.read().split('\n\n')
    for f in data[0].splitlines():
        m = field_re.match(f)
        fields.append(Ticket_Field(m.group(1), range(int(m.group(2)), int(
            m.group(3)) + 1), range(int(m.group(4)), int(m.group(5)) + 1)))
    my_ticket = [int(i) for i in data[1].splitlines()[1].split(',')]

    nearby_tickets = []
    for t in data[2].splitlines()[1:]:
        nearby_tickets.append([int(i) for i in t.split(',')])

error_rate = 0
for i in range(len(nearby_tickets) - 1, -1, -1):
    for v in nearby_tickets[i]:
        if not any([v in f.range1 or v in f.range2 for f in fields]):
            error_rate += v
            del nearby_tickets[i]

print("P1:\n{}".format(error_rate))

# field, ticket value idx, ticket
vmat = np.zeros((len(fields), len(fields), len(nearby_tickets)))
for i in range(len(fields)):
    for j in range(len(nearby_tickets)):
        for k in range(len(fields)):
            vmat[i, k, j] = nearby_tickets[j][k] in fields[i].range1 or nearby_tickets[j][k] in fields[i].range2

possible_indicies = [set() for _ in range(len(fields))]
for i in range(len(vmat)):
    for j in range(len(vmat[i])):
        if all(vmat[i][j]):
            possible_indicies[i].add(j)

actual_indicies = [None] * len(possible_indicies)

while(not all(e is not None for e in actual_indicies)):
    for i in range(len(possible_indicies)):
        if(len(possible_indicies[i]) == 1):
            val = list(possible_indicies[i])[0]
            actual_indicies[i] = val
            for j in range(len(possible_indicies)):
                possible_indicies[j].discard(val)

depature_mul = 1
for i in range(len(actual_indicies)):
    if fields[i].name.startswith("departure"):
        depature_mul *= my_ticket[actual_indicies[i]]

print("P2:\n{}".format(depature_mul))
