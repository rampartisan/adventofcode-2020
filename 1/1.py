import os
import itertools


def check_sum_and_get_product(entry_indicies, target_sum):
    sum = 0
    for i in entry_indicies:
        sum += expense_report[i]
    if(sum == target_sum):
        product = 1
        print("Entries:", end='')
        for i in entry_indicies:
            product *= expense_report[i]
            print(" {} ({})".format(i, expense_report[i]), end='')
        print(" sum to {}, with the product {}".format(target_sum, product))


def print_product_of_n_expenses_that_sum_to_x(n, x, range_list=[], current_depth=0, entry_indicies=[]):
    if len(range_list) != n:
        range_list  = []
        for _ in range(n):
            range_list.append(range(len(expense_report)))

    if entry_indicies == []:
        entry_indicies = [0]*n
    if current_depth == n-1:
        for entry_indicies[current_depth] in range_list[current_depth]:
            check_sum_and_get_product(entry_indicies, x)
    else:
        for entry_indicies[current_depth] in range_list[current_depth]:
            print_product_of_n_expenses_that_sum_to_x(n, x, entry_indicies=entry_indicies, range_list=range_list,
                                                      current_depth=current_depth+1)


script_dir = os.path.dirname(__file__)
with open(script_dir+"/1.txt") as f:
    expense_report = [int(i) for i in f.read().splitlines()]

print("P1:")
print_product_of_n_expenses_that_sum_to_x(2, 2020)
print("P2:")
print_product_of_n_expenses_that_sum_to_x(3, 2020)
