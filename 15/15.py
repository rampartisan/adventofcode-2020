def play_counting_game(starting_nums, target_count):
    nums = dict(zip(starting_nums, range(1, len(starting_nums))))
    last_spoken = starting_nums[-1]
    count = len(starting_nums)
    while(count <= target_count - 1):
        spoken_diff = count - nums[last_spoken] if last_spoken in nums else 0
        nums[last_spoken] = count
        last_spoken = spoken_diff
        count += 1
    return last_spoken


def test():
    starting_nums = [[0, 3, 6], [1, 3, 2], [2, 1, 3],
                     [1, 2, 3], [2, 3, 1], [3, 2, 1], [3, 1, 2]]
    target_number = [2020, 2020, 2020, 2020, 2020, 2020, 2020]
    expected = [436, 1, 10, 27, 78, 438, 1836]

    for i in range(len(starting_nums)):
        result = play_counting_game(starting_nums[i], target_number[i])
        assert result == expected[i], "expected {} got {}".format(
            expected[i], result)


test()

print("P1:\n{}".format(play_counting_game([1, 0, 15, 2, 10, 13], 2020)))
print("P2:\n{}".format(play_counting_game(
    [1, 0, 15, 2, 10, 13], 30000000)))  # SLOOOOOOOW
