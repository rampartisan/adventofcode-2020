import os

groups = []

script_dir = os.path.dirname(__file__)
with open(script_dir+'/6.txt') as f:
    gr = [x.split() for x in f.read().split('\n\n')]
    for g in gr:
        groups.append([])
        for m in g:
            groups[len(groups) - 1].append(set({char for char in m}))

sum_unique_answers_per_group = 0
sum_common_answers_per_group = 0
for g in groups:
    sum_unique_answers_per_group += len(set.union(*g))
    sum_common_answers_per_group += len(set.intersection(*g))

print("P1:\n{}".format(sum_unique_answers_per_group))
print("P2:\n{}".format(sum_common_answers_per_group))
