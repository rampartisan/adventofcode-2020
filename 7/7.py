import os
import networkx as nx

script_dir = os.path.dirname(__file__)
remove_words = ['bags', 'bag', 'contain', 'no', 'other']

graph = nx.DiGraph()

with open(script_dir+"/7.txt") as f:
    raw = [x.replace(',', '').replace('.', '').split(' ')
           for x in f.read().splitlines()]
    for n in raw:
        n = [x for x in n if not x in remove_words]
        for i in range(2, len(n), 3):
            graph.add_edge(n[0] + n[1], n[i + 1] + n[i + 2], weight=int(n[i]))

print("P1:\n{}".format(len(nx.ancestors(graph, 'shinygold'))))


def count_bags_in_node(node):
    bags = 0
    for node_name, node_attr in graph[node].items():
        bags += node_attr['weight'] * \
            count_bags_in_node(node_name) + node_attr['weight']
    return bags


print("P2:\n{}".format(count_bags_in_node('shinygold')))
