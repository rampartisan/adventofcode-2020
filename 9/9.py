import os
from collections import deque

script_dir = os.path.dirname(__file__)
with open(script_dir+"/9.txt") as f:
    raw = [int(x) for x in f.read().splitlines()]
    cipher = raw[:25]
    XMAS = raw[25:]


def find_numbers_in_list_that_sum_to_x(input_list, x):
    numbers = set(input_list)
    for n in numbers:
        if (x - n) in numbers:
            return (n, x-n)
    return None


def find_invalid_numbers(cipher, input_list):
    cipherq = deque(cipher)
    weak_nums = []
    index = 0
    for x in XMAS:
        if find_numbers_in_list_that_sum_to_x(list(cipherq), x) is None:
            weak_nums.append((x, index))
        cipherq.popleft()
        cipherq.append(x)
        index += 1
    return weak_nums


weak_number = find_invalid_numbers(cipher, XMAS)[0][0]

print("P1:\n{}".format(weak_number))


def find_weakness(weak_num, input_list):
    for i in range(len(input_list)):
        tot = XMAS[i]
        next_i = i+1
        while tot < weak_number and next_i < len(input_list):
            tot += input_list[next_i]
            next_i += 1
        if tot == weak_number:
            contig = input_list[i:next_i-1]
            return min(contig) + max(contig)


print("P2:\n{}".format(find_weakness(weak_number, XMAS)))
