import os
import re
import numpy as np
import itertools


class Tile():
    def __init__(self, id, data):
        self.id = id
        self.data = np.array(data)
        self.neighbours = set()

    def get_id(self):
        return self.id

    def get_data(self):
        return self.data

    def get_edges(self):
        # top,right,bottom,left
        return  [self.data[0, :], self.data[:, -1], self.data[-1, :], self.data[:, 0]]

    def get_edges_string(self):
        return [''.join(x) for x in self.get_edges()]

    def add_neighbour(self, n):
        self.neighbours.add(n)

    def get_neighbours(self):
        return self.neighbours
    
    def rotate_90(self, num_times):
        self.data = np.rot90(self.data, num_times)

    def flip(self):
        self.data = np.flip(data)


tiles = []
script_dir = os.path.dirname(__file__)
with open(script_dir+"/20.txt") as f:
    raw_tile = f.read().split('\n\n')
    for t in raw_tile:
        header, data = t.split('\n', 1)
        id = int(re.search(r'(\d+)', header).group(1))
        data = [list(x) for x in data.split()]
        tiles.append(Tile(id, data))

for t1,t2 in itertools.combinations(tiles,2):
    for _ in range(4):
        match = set(t1.get_edges_string()) & set(t2.get_edges_string())
        if match:
            t1.add_neighbour(t2)
            t2.add_neighbour(t1)
        t1.rotate_90(1)

corners = [t for t in tiles if len(t.get_neighbours()) == 2]
corner_ids = [t.get_id() for t in corners]
print("P1:\n{}".format(np.prod(np.array(corner_ids))))