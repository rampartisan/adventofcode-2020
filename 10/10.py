import os
import numpy as np

script_dir = os.path.dirname(__file__)
with open(script_dir+"/10.txt") as f:
    adapters = sorted([int(x) for x in f.read().splitlines()])

adapters.insert(0, 0)
adapters.append(adapters[-1]+3)

diff = list(np.diff(adapters))
print("P1:\n{}".format(diff.count(1) * diff.count(3)))

# dont need to count from outlet, cover it in the dict init below
adapters = adapters[1:]

# number of paths from each adapter from the (up to) 3 before
paths = {0: 1}
for a in adapters:
    paths[a] = paths.get(a - 3, 0) + paths.get(a - 2, 0) + paths.get(a - 1, 0)

print("P2:\n{}".format(paths[adapters[-1]]))
