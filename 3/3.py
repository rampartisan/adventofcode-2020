import os


def get_product_of_trees_hit_for_slopes(slopes):
    num_tree_hit_product = 1
    for s in slopes:
        x = 0
        y = 0
        hit_bottom = False
        num_trees_hit = 0
        while not hit_bottom:
            x += s[0]
            x %= slope_map_width - 1

            y += s[1]

            if(slope_map[y][x] == '#'):
                num_trees_hit += 1

            if(y >= slope_map_height - 1):
                hit_bottom = True
                num_tree_hit_product *= num_trees_hit
    return num_tree_hit_product


script_dir = os.path.dirname(__file__)
slope_map = []
with open(script_dir+"/3.txt") as f:
    for line in f:
        slope_map.append([char for char in line])

slope_map_width = len(slope_map[0])
slope_map_height = len(slope_map)

print("Parsed {} by {} map".format(slope_map_width, slope_map_height))
slopes_p1 = [(3, 1)]
print("P1\n{}".format(get_product_of_trees_hit_for_slopes(slopes_p1)))
slopes_p2 = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
print("P2\n{}".format(get_product_of_trees_hit_for_slopes(slopes_p2)))
