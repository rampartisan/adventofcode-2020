import os
import re
import random


def get_passports_from_file(file):
    passports = []
    with open(file) as f:
        raw_passports = f.read().split('\n\n')
        for i in range(len(raw_passports)):
            raw_passports[i] = raw_passports[i].strip().replace('\n', ' ')
            raw_passports[i] = raw_passports[i].split(' ')
            passports.append({})
            for field in raw_passports[i]:
                key, value = field.split(':')
                passports[len(passports) - 1][key] = value
    return passports


def is_valid_byr(byr: str):
    # ranges with +1 because python range is exclusive of end
    return int(byr) in range(1920, 2003)


def is_valid_iyr(iyr: str):
    return int(iyr) in range(2010, 2021)


def is_valid_eyr(eyr: str):
    return int(eyr) in range(2020, 2031)


def get_hgt_as_cm(hgt: str):
    height_re = re.compile('([0-9]+)([a-z]+)?')
    hgt_match = height_re.search(hgt)
    if(hgt_match.group(2) == 'in'):
        return round(int(hgt_match.group(1))*2.54)
    elif(hgt_match.group(2) == 'cm'):
        return int(hgt_match.group(1))
    else:
        return 0


def is_valid_hgt(hgt: str):
    real_hgt = int(get_hgt_as_cm(hgt))
    return real_hgt in range(150, 194)


def is_valid_hcl(hcl: str):
    hex_re = re.compile('^#([0-9a-f]{6})$')
    return hex_re.search(hcl) is not None


def is_valid_ecl(ecl: str):
    valid_ecl = ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']
    return any(ecl in s for s in valid_ecl)


def is_valid_pid(pid: str):
    pid_re = re.compile('^([0-9]{9})$')
    return pid_re.search(pid) is not None


field_validators = [(is_valid_byr, 'byr'), (is_valid_iyr, 'iyr'), (is_valid_eyr, 'eyr'),
                    (is_valid_hgt, 'hgt'), (is_valid_hcl, 'hcl'), (is_valid_ecl, 'ecl'), (is_valid_pid, 'pid')]


def passport_has_valid_fields(passport):
    random.shuffle(field_validators)
    for v in field_validators:
        if not (v[0](passport[v[1]])):
            return False
    return True


script_dir = os.path.dirname(__file__)
passports = get_passports_from_file(script_dir+'/4.txt')
valid_passports = []
required_fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
# only add passports containing all required fields
for p in passports:
    has_required_fields = True
    for f in required_fields:
        if not f in p:
            has_required_fields = False
    if has_required_fields:
        valid_passports.append(p)

print("P1:\n{}".format(len(valid_passports)))

valid_passports[:] = [
    p for p in valid_passports if passport_has_valid_fields(p)]

print("P2:\n{}".format(len(valid_passports)))


def test_is_valid_byr():
    # 1920-2002
    assert is_valid_byr(1920)
    assert is_valid_byr(2002)
    assert not is_valid_byr(2003)
    assert not is_valid_byr(1919)


def test_is_valid_iyr():
    # 2010-2020
    assert is_valid_iyr(2010)
    assert is_valid_iyr(2012)
    assert is_valid_iyr(2020)
    assert not is_valid_iyr(2021)
    assert not is_valid_iyr(2009)


def test_is_valid_eyr():
    # 2020-2030
    assert is_valid_eyr(2020)
    assert is_valid_eyr(2030)
    assert not is_valid_eyr(2019)
    assert not is_valid_eyr(2031)


def test_is_valid_hgt():
    # a number followed by either cm or in
    # if cm, the number must be at least 150 and at most 193
    assert is_valid_hgt('150cm')
    assert is_valid_hgt('193cm')
    assert not is_valid_hgt('149cm')
    assert not is_valid_hgt('194cm')
    # If in, the number must be at least 59 and at most 76
    assert is_valid_hgt('59in')
    assert is_valid_hgt('76in')
    assert not is_valid_hgt('58in')
    assert not is_valid_hgt('77in')
    # check that not having in/cm fails (specs vauge, think this is the case)
    assert not is_valid_hgt('59')
    assert not is_valid_hgt('150')


def test_is_valid_hcl():
    # a # followed by exactly six characters 0-9 or a-f
    assert is_valid_hcl('#000000')
    assert is_valid_hcl('#aaaaaa')
    assert is_valid_hcl('#999999')
    assert is_valid_hcl('#ffffff')
    assert is_valid_hcl('#0a0a0a')
    assert not is_valid_hcl('#0a0a0z')  # z fail
    assert not is_valid_hcl('#0')  # too short
    assert not is_valid_hcl('#0a0a0af')  # too long
    assert not is_valid_hcl('0a0a0a')  # no # prefix


def test_is_valid_ecl():
    # exactly one of: amb blu brn gry grn hzl oth
    valid_ecl = ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']
    for c in valid_ecl:
        assert is_valid_ecl(c)
    assert not is_valid_ecl('svart')
    assert not is_valid_ecl('amber')  # extension of amb, should fail


def test_is_valid_pid():
    # a nine-digit number, including leading zeroes
    assert is_valid_pid('123456789')
    assert is_valid_pid('000000009')
    assert not is_valid_pid('12345678910')
    assert not is_valid_pid('0')
    assert not is_valid_pid('1a2b3c4d5')


def test_is_valid_passport():
    invalid = get_passports_from_file(script_dir+'/test_invalid.txt')
    for p in invalid:
        assert not passport_has_valid_fields(p)

    valid = get_passports_from_file(script_dir+'/test_valid.txt')
    for p in valid:
        assert passport_has_valid_fields(p)


test_is_valid_byr()
test_is_valid_iyr()
test_is_valid_eyr()
test_is_valid_hgt()
test_is_valid_hcl()
test_is_valid_ecl()
test_is_valid_pid()
test_is_valid_passport()
