import os
import re
from collections import namedtuple

script_dir = os.path.dirname(__file__)
with open(script_dir+"/2.txt") as f:
    corrupted_db = f.read().splitlines()

Rule_Pwd = namedtuple('Rule_Pwd', 'letter min_count max_count pwd')
rule_pwd_list = []
for p in corrupted_db:
    expr = re.compile("([0-9]+)-([0-9]+)\s([a-z]):\s([a-z]+)")
    match = expr.search(p)
    rule_pwd_list.append(Rule_Pwd(match.group(3), int(
        match.group(1)), int(match.group(2)), match.group(4)))

correct_count = 0

for p in rule_pwd_list:
    letter_count = p.pwd.count(p.letter)
    if p.min_count <= letter_count <= p.max_count:
        correct_count += 1

print("P1:\n{}".format(correct_count))

correct_count = 0

for p in rule_pwd_list:
    pos_1 = False
    pos_2 = False

    if p.pwd[p.min_count - 1] == p.letter:
        pos_1 = True

    if p.pwd[p.max_count - 1] == p.letter:
        pos_2 = True

    if(pos_1 != pos_2):
        correct_count += 1

print("P2:\n{}".format(correct_count))
