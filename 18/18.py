import queue
import itertools
import re
import os


def add(a, b):
    return a+b


def mul(a, b):
    return a*b


def find_paren_groups(s):
    q = queue.LifoQueue()
    c = 0
    groups = []
    for i in range(len(s)):
        if s[i] == '(':
            q.put(i)
        if s[i] == ')':
            idx = q.get()
            if q.empty():
                groups.append([c, idx, i])
                c += 1
    return groups


def mafs(in_exp, addition_first):
    paren_group_idx = find_paren_groups(in_exp)
    parens = [str(mafs(in_exp[p[1]+1:p[2]], addition_first))
              for p in paren_group_idx]
    for p in paren_group_idx[::-1]:
        in_exp = in_exp[:p[1]] + parens[p[0]] + in_exp[p[2]+1:]

    nums = [int(x) for x in re.findall(r'(\d+)', in_exp)]
    ops = re.findall(r'(\+|\*)', in_exp)

    if addition_first:
        adds = [i for i, x in enumerate(ops) if x == "+"]
        ops = [op for op in ops if op != "+"]
        for i in adds[::-1]:
            nums = nums[:i] + [add(nums[i],nums[i+1])] + nums[i+2:]
                
    a = nums[0]
    for i in range(1, len(nums)):
        if ops[i - 1] == '+':
            a = add(a, nums[i])
        else:
            a = mul(a, nums[i])
    return a


assert(mafs("1 + 2 * 3 + 4 * 5 + 6", False) == 71)
assert(mafs("1 + 2 * 3 + 4 * 5 + 6", True) == 231)
assert(mafs("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", False) == 13632)
assert(mafs("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", True) == 23340)

with open(os.path.dirname(__file__)+"/18.txt") as f:
    exp = f.read().splitlines()

results = [mafs(e, False) for e in exp]
print("P1:\n{}".format(sum(results)))
results = [mafs(e, True) for e in exp]
print("P2:\n{}".format(sum(results)))