import os
import math
import numpy as np
from functools import reduce

# https://github.com/MischaDy/PyAdventOfCode2020/blob/main/day%2013/day13_part2.py
# super nice implementaton :)


def chinese_remainder(remainders, divisors):
    M = prod(divisors)
    as_ = list(map(lambda d: int(M / d), divisors))
    eea_results = map(lambda tup: extended_gcd(*tup), zip(as_, divisors))
    is_ = [result[0] % div for result, div in zip(eea_results, divisors)]
    Z = sum(map(prod, zip(is_, remainders, as_)))
    x = Z % M
    return x


def prod(nums):
    return reduce(lambda a, b: a * b, nums, 1)


def extended_gcd(a, b):
    old_r, r = a, b
    old_s, s = 1, 0
    old_t, t = 0, 1

    while r != 0:
        quotient = old_r // r
        old_r, r = r, old_r - quotient * r
        old_s, s = s, old_s - quotient * s
        old_t, t = t, old_t - quotient * t

    return old_s, old_t, old_r


def find_closest_number_greater_than_n_and_div_by_m(n, m):
    return m * (int(n/m) + 1)


script_dir = os.path.dirname(__file__)
with open(script_dir+"/13.txt") as f:
    timestamp = int(f.readline())
    bus = [int(i) for i in f.readline().replace('x', '-1').strip().split(',')]


bus_no_missing = [b for b in bus if b != -1]
timetable = [(find_closest_number_greater_than_n_and_div_by_m(
    timestamp, b) - timestamp, b) for b in bus_no_missing]
min_wait_bus = min(timetable)
print("P1:\n{}".format(min_wait_bus[0] * min_wait_bus[1]))


rem = []
div = []

for i in range(len(bus)):
    if bus[i] == -1:
        continue
    rem.append(-i % bus[i])
    div.append(bus[i])

print("P2:\n{}".format(chinese_remainder(rem, div)))
