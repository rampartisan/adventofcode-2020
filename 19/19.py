import os
import queue
import re
from typing import List
import copy

script_dir = os.path.dirname(__file__)


def get_rules_and_messages_from_file(file):
    with open(file) as f:
        data = f.read().split('\n\n')
        rules = {}
        for r in data[0].splitlines():
            k, v = r.split(': ')
            k = int(k)
            if v[0] == '"':
                v = v.replace('"', '')
            else:
                vv = []
                for sub in v.split('|'):
                    vv.append([int(x) for x in sub.split()])
                v = vv
            rules[k] = v
        messages = data[1].splitlines()
    return rules, messages


def check_message_for_rule(message, rules, rule, depth=0):
    if depth >= len(message):
        return []

    if rule[0] == 'a' or rule[0] == 'b':
        if message[depth] == rule[0]:
            return [depth + 1]
        else:
            return []

    valid = []
    for sub_rule in rule:
        s_valid = [depth]

        for r in sub_rule:
            n_valid = []
            for i in s_valid:
                n_valid += check_message_for_rule(message, rules, rules[r], i)
            s_valid = n_valid

        valid += s_valid

    return valid

rules, mess = get_rules_and_messages_from_file(script_dir+"/19.txt")
amend_rules = copy.deepcopy(rules)
amend_rules[8] = [[42], [42,8]]
amend_rules[11] = [[42,31], [42,11,31]]
valid_count = 0
valid_count_amend = 0
for m in mess:
    if len(m) in check_message_for_rule(m, rules, rules[0]):
        valid_count += 1
    if len(m) in check_message_for_rule(m, amend_rules, amend_rules[0]):
        valid_count_amend += 1

print("P1:\n{}".format(valid_count))
print("P2:\n{}".format(valid_count_amend))

