import os
import numpy as np
import math


def pol2car(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return[x, y]


def car2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return[rho, phi]


def set_sail_to_nowhere():
    vecs = []
    script_dir = os.path.dirname(__file__)
    with open(script_dir+"/12.txt") as f:
        cumulative_angle = 0  # start east
        for d in f.read().splitlines():
            direc = d[0]
            mag = int(d[1:])
            if direc == 'N':
                vecs.append([0, mag])
            if direc == 'S':
                vecs.append([0, -mag])
            if direc == 'E':
                vecs.append([mag, 0])
            if direc == 'W':
                vecs.append([-mag, 0])
            if direc == 'F':
                vecs.append(pol2car(mag, math.radians(cumulative_angle)))
            if direc == 'L':
                cumulative_angle += mag
            if direc == 'R':
                cumulative_angle += 360-mag

    cumulative_vec = np.sum(np.array(vecs), axis=0)
    return round(abs(cumulative_vec[0]) + abs(cumulative_vec[1]))


def set_sail_to_waypoint(wp_vec):
    ship_vec = [0, 0]
    script_dir = os.path.dirname(__file__)
    with open(script_dir+"/12.txt") as f:
        for d in f.read().splitlines():
            direc = d[0]
            mag = int(d[1:])
            if direc == 'N':
                wp_vec[1] += mag
            if direc == 'S':
                wp_vec[1] -= mag
            if direc == 'E':
                wp_vec[0] += mag
            if direc == 'W':
                wp_vec[0] -= mag
            if direc == 'F':
                ship_vec[0] += mag * wp_vec[0]
                ship_vec[1] += mag * wp_vec[1]
            if direc == 'L':
                wp_pol = car2pol(wp_vec[0], wp_vec[1])
                wp_pol[1] += math.radians(mag)
                wp_vec = pol2car(wp_pol[0], wp_pol[1])
            if direc == 'R':
                wp_pol = car2pol(wp_vec[0], wp_vec[1])
                wp_pol[1] += math.radians(360-mag)
                wp_vec = pol2car(wp_pol[0], wp_pol[1])

    return round(abs(ship_vec[0]) + abs(ship_vec[1]))


print("P1:\n{}".format(set_sail_to_nowhere()))
print("P2:\n{}".format(set_sail_to_waypoint([10, 1])))
