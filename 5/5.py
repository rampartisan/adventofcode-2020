import os
import re
import math

script_dir = os.path.dirname(__file__)
bp_re = re.compile('^([FB]{7})([RL]{3})$')
bp_db = []
with open(script_dir+"/5.txt") as f:
    raw = f.read().splitlines()
    for bp in raw:
        match = bp_re.search(bp)
        assert(match)
        bp_db.append((match.group(1), match.group(2)))


def get_seat_id_from_bp(bp, start_row, end_row, start_col, end_col):
    for x in bp[0][:-1]:
        row_range = math.ceil((end_row - start_row) / 2)
        if(x == 'F'):
            end_row -= row_range
        else:
            start_row += row_range
    row = start_row if bp[0][-1] == 'F' else end_row

    for y in bp[1][:-1]:
        col_range = math.ceil((end_col - start_col) / 2)
        if(y == 'L'):
            end_col -= col_range
        else:
            start_col += col_range
    col = start_col if bp[1][-1] == 'L' else end_col
    return (row * 8) + col


occupied_seats = set()
for bp in bp_db:
    occupied_seats.add(get_seat_id_from_bp(bp, 0, 127, 0, 7))

seats = set(range(min(occupied_seats), max(occupied_seats) + 1))
free_seat = list(seats.difference(occupied_seats))[
    0]  # should probably check len not > 1
print("P1:\n{}".format(max(occupied_seats)))
print("P2:\n{}".format(free_seat))


def test_get_seat_id():
    test_bps = [('FBFBBFF', 'RLR'), ('BFFFBBF', 'RRR'), ('FFFBBBF', 'RRR'),
                ('BBFFBBF', 'RLL'), ('FFFFFFF', 'LLL'), ('BBBBBBB', 'RRR')]
    expected_ids = [357, 567, 119, 820, 0, 1023]
    for i in range(len(test_bps)):
        assert get_seat_id_from_bp(
            test_bps[i], 0, 127, 0, 7) == expected_ids[i]


test_get_seat_id()
