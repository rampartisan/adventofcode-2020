import os
from collections import namedtuple

Instruction = namedtuple('Instruction', 'op val')
Exit_Code = namedtuple('Exit_Code', 'code val')

script_dir = os.path.dirname(__file__)
with open(script_dir+"/8.txt") as f:
    prog = [Instruction(x[0], int(x[1]))
            for x in (y.split(' ') for y in f.read().splitlines())]


def run_program(program):
    acc = 0
    iptr = 0
    executed_instructions = set()

    while True:

        # terminate with error if we are in a loop
        if(iptr in executed_instructions):
            return Exit_Code(1, acc)

        # terminate with error if we have run >+1 past instruction length
        if(iptr > len(program)):
            return Exit_Code(2, acc)

        # terminate with sucess if we have run +1 past instruction length
        if(iptr == len(program)):
            return Exit_Code(0, acc)

        executed_instructions.add(iptr)

        instr = program[iptr]
        if(instr.op == 'acc'):
            acc += instr.val
            iptr += 1
        elif(instr.op == 'jmp'):
            iptr += instr.val
        elif(instr.op == 'nop'):
            iptr += 1


# just assume we exited with code 1 (inf loop)
print("P1:\n{}".format(run_program(prog).val))

# collect all jmp/nops
nop_and_jmp_idx = [i for i in range(
    len(prog)) if prog[i].op == 'jmp' or prog[i].op == 'nop']

proper_terminate_val = -1

for i in nop_and_jmp_idx:

    # swap the nop/jmp instruction
    prog[i] = Instruction(
        'jmp', prog[i].val) if prog[i].op == 'nop' else Instruction('nop', prog[i].val)

    result = run_program(prog)
    if(result.code == 0):
        proper_terminate_val = result.val
        break

    # swap the nop/jmp instruction back
    prog[i] = Instruction(
        'jmp', prog[i].val) if prog[i].op == 'nop' else Instruction('nop', prog[i].val)

if(proper_terminate_val != -1):
    print("P2:\n{}".format(proper_terminate_val))
else:
    print("Failed P2...")
