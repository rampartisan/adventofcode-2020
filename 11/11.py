import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"

import math
import copy
from time import sleep
import numpy as np
import pygame

pygame.init()

script_dir = os.path.dirname(__file__)
seats = []
with open(script_dir+"/11.txt") as f:
    for line in f:
        line = line.replace('\n', '')
        seats.append([char for char in line])


def get_num_occupied_seats(seat_map):
    oc = 0
    for row in seat_map:
        oc += row.count('#')
    return oc


view_dirs = [(-1, -1), (0, -1), (1, -1), (-1, 0),
                 (1, 0), (-1, 1), (0, 1), (1, 1)]


def get_number_of_viewable_occupied_seats(seat_map, view_distance, x, y):

    num_seats = 0
    max_x = len(seat_map[0]) - 1  if len(seat_map[0]) - 1 < x + view_distance else x + view_distance
    max_y = len(seat_map) - 1if len(seat_map) - 1 < y + view_distance else y + view_distance


    for d in view_dirs:
        
        curr_x = x
        curr_y = y
        moved_distance = 0
        while(True):

            moved_distance += 1
            if moved_distance > view_distance:
                break

            curr_x += d[0]
            curr_y += d[1]
            
            if(curr_x < 0 or curr_x > max_x):
                break
            if(curr_y  < 0 or curr_y  > max_y):
                break
            
            if(seat_map[curr_y][curr_x] == 'L'):
                break

            if(seat_map[curr_y][curr_x] == '#'):
                num_seats += 1
                break
            


    return num_seats


def run_game_of_seats(seat_map, num_occupied_to_be_empty, seat_distance):
    changed = True

    state_colour_map = {'#': (156, 93, 56),
                        'L': (196, 178, 116),
                        '.': (57, 100, 119)}

    for x in range(x_max):
        for y in range(y_max):
            colour = state_colour_map[seat_map[y][x]]
            xd = x * (cell_size + padding)
            xy = y * (cell_size + padding)
            pygame.draw.rect(screen, colour, [xd, xy, cell_size, cell_size], 0)

    pygame.display.flip()

    while(changed):
        changed = False
        changes = []

        for x in range(x_max):
            for y in range(y_max):
                state = seat_map[y][x]
                num_view_occ = get_number_of_viewable_occupied_seats(
                    seat_map, seat_distance, x, y)
                if(state == 'L' and num_view_occ == 0):
                    changes.append((x, y))
                if(state == '#' and num_view_occ >= num_occupied_to_be_empty):
                    changes.append((x, y))

        for c in changes:
            new_state = 'L' if seat_map[c[1]][c[0]] == '#' else '#'
            seat_map[c[1]][c[0]] = new_state
            colour = state_colour_map[new_state]
            xd = c[0] * (cell_size + padding)
            xy = c[1] * (cell_size + padding)
            pygame.draw.rect(screen, colour, [xd, xy, cell_size, cell_size], 0)

        if changes:
            changed = True

        pygame.display.flip()
        sleep(0.05)

    return get_num_occupied_seats(seat_map)


cell_size = 9
padding = 1
x_max = len(seats[0])
y_max = len(seats)
screen = pygame.display.set_mode(
    (x_max * (padding + cell_size), y_max * (padding + cell_size)), 0, 32)

screen.fill((19, 42, 63))

p1_seats = copy.deepcopy(seats)
p2_seats = copy.deepcopy(seats)

print("P1:\n{}".format(run_game_of_seats(p1_seats, 4, 1)))
print("P2:\n{}".format(run_game_of_seats(p2_seats, 5, math.inf)))

pygame.quit()
