import os
import re
from collections import namedtuple
from bitarray import bitarray
import itertools

Mask_Bit = namedtuple('Mask_Bit', 'index val')
mem_re = re.compile('^mem\[(\d+)]\s=\s(\d+)$')


def p1():
    mem = {}
    script_dir = os.path.dirname(__file__)
    with open(script_dir+"/14.txt") as f:
        for l in f.read().splitlines():
            if l[:3] == 'mas':
                mask = []
                for i in range(36):
                    if(l[i+7] == 'X'):
                        continue
                    m = i
                    v = int(l[i+7])
                    mask.append(Mask_Bit(m, v))

            else:
                match = mem_re.search(l)
                address = match.group(1)
                val_bits = bitarray(bin(int(match.group(2)))[2:].zfill(36))

                for m in mask:
                    val_bits[m.index] = m.val

                mem[address] = val_bits

    sum = 0
    for b in mem.values():
        sum += int(b.to01(), 2)
    return sum


def p2():
    mem = {}
    script_dir = os.path.dirname(__file__)
    with open(script_dir+"/14.txt") as f:
        for l in f.read().splitlines():
            if l[:3] == 'mas':
                mask = []
                for i in range(36):
                    if(l[i+7] == 'X'):
                        v = -1
                    else:
                        v = int(l[i+7])
                    m = i
                    mask.append(Mask_Bit(m, v))

            else:
                match = mem_re.search(l)
                adr = bitarray(bin(int(match.group(1)))[2:].zfill(36))
                val = match.group(2)
                floating = []
                for m in mask:
                    if m.val == -1:
                        floating.append(
                            [Mask_Bit(m.index, 0), Mask_Bit(m.index, 1)])
                    elif m.val == 1:
                        adr[m.index] = 1

                floating_combinations = itertools.product(*floating)
                for combination in floating_combinations:
                    tmp_addr = adr
                    for b in combination:
                        tmp_addr[b.index] = b.val
                    mem[int(tmp_addr.to01(), 2)] = int(val)

    sum = 0
    for b in mem.values():
        sum += b
    return sum


print("P1:\n{}".format(p1()))
print("P2:\n{}".format(p2()))
